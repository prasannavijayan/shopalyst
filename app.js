// $(function() {


// 	let options = {
// 		body: {
// 			apikey: ApiKey
// 		},
// 		headers: {
// 			"Content-Type": "application/json"
// 		}
// 	}
// 	fetch(FetchUrl, options).then((response) => {

// 	}, (err) => {
// 		console.log(err);
// 	})
// });

var app = angular.module("www", []);
app.controller("appCtrl", function($scope, $http) {

	const BaseUrl = "http://api.shortlyst.com/v1/products";
	const ApiKey = "e3ebd9fdf9704775c7fd6bb4f20e1798";
	const FetchUrl = `${BaseUrl}?apikey=${ApiKey}&limit=24`;

	$scope.responses = [];

	let config = {
		body: { },
		headers: { "Content-Type": "application/json" }
	}

	function responseCallBack(response) {
		$scope.responses = response.data;
	}

	function errorCallBack(err) {
		$scope.error = err;
	}

  $http.get(FetchUrl, config).then(responseCallBack, errorCallBack);
});